<div>
	<img src="laravel.png" width="200px">
</div>

## DB log in details.

	DB information is provided in the .env.example file.

## Admin Login.

	User: admin@admin.nl
	Password: admin123
	
	Password is required to be of length >= 6.
	
	Small description:
		Clicking on the user in the top right of the navigation bar will display a drop down, with
		for admins two extra options, one for exporting the registered users to excel and one to go
		to the admin panel.
		
		On the admin panel, the admin has 2 buttons routing the admin to either a users overview
		or the recipes overview where the admin can either read, update or delete users/recipes.

## Project members

Full name | Student number
--------- | --------------
Remy Bos | 2158030
Bas Joosten | 3464555

## Project description

TO BE UPDATED.

## Live website

The url below provides a link to the most recent version of the web application.

[Link to the live website.](http://i231896.hera.fhict.nl/web-3/)