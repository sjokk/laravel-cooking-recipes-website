<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;  
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view() : View
    {
        // In case we only want public accs.
        // User::where('is_public', 1)->get()
        return view('UserController.user-table', [
                'users' => User::all()
            ]);
    }
}
