<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Recipe extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [ 
            'type' => 'recipes',
            'id' => $this->id,
            'attributes' => [
                 'title' => $this->title,
                 'ingredients' => $this->ingredients,
                 'directions' => $this->directions,
                 'description' => $this->description,
                 'totalTime' => $this->totalTime
            ],
            'links' => [
                 'self' => route('recipes.show', $this->id),
            ],
        ];
    }
}
