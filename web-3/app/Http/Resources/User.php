<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
       return [ 
           'type' => 'users',
           'id' => $this->id,
           'attributes' => [
                'name' => $this->name,
                'email' => $this->email,
                'is_public' => $this->is_public,
                'created_at' => $this->created_at->format('Y-m-d'),
           ],
           'links' => [
                'self' => route('users.show', $this->id),
           ],
       ];
    }
}
