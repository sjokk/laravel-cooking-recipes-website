<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Http\Resources\Recipe as RecipeResource;
use App\Http\Requests;
use App\User;

class RecipeApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get recipes
        $recipes = Recipe::paginate(3);

        //return recipes collection
        return RecipeResource::collection($recipes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $recipe = new Recipe;
        $user = User::find($request->input('user_id'));
        // Check if for this user an api_token exists otherwise return unauthorized.
        if ($user !== null) { 
            if ($user->api_token !== null) {
                if ($user->api_token === $request->api_token) {
                    $recipe->title = $request->input('title');
                    $recipe->image = 'www.test.com';
                    $recipe->description = $request->input('description');
                    $recipe->directions = $request->input('directions');
                    $recipe->ingredients = $request->input('ingredients');
                    $recipe->preperationTime = '00:20';
                    $recipe->cookingTime = '00:30';
                    $recipe->totalTime = $request->input('totalTime');
                    $recipe->servings = $request->input('servings');
                    $recipe->calories = $request->input('calories');
                    $recipe->cover_image = 'NoImage.jpg';
                    $recipe->cover_image_pixel = 'NoImage.jpg';
                    $recipe->user_id = $request->input('user_id');
                    
                    if($recipe->save()) {
                        return new RecipeResource($recipe);
                    }
                }
            }
            return response()->json([
                'error' => true,
                'msg' => 'Unauthorized',
            ], 401);
        }
        return response()->json([
            'error' => true,
            'msg' => 'User with id '. $request->input('user_id') .' was not found.',
        ], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe = Recipe::findOrFail($id);
        return new RecipeResource($recipe);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $recipe = Recipe::find($id);
        if ($recipe !== null) {
            // Only update fields that are provided and with values.
            // No clue if there is any way of validating this.
            $user = User::find($request->input('user_id'));
            // Check if for this user an api_token exists otherwise return unauthorized.
            if ($user !== null) { 
                if ($user->api_token !== null) {
                    if ($user->api_token === $request->api_token) {
                        if ($request->filled('title')) {
                            $recipe->title = $request->input('title');
                        }
                        if ($request->filled('description')) {
                            $recipe->description = $request->input('description');
                        }
                        if ($request->filled('directions')) {
                            $recipe->directions = $request->input('directions');
                        }
                        if ($request->filled('ingredients')) {
                            $recipe->ingredients = $request->input('ingredients');
                        }
                        if ($request->filled('totalTime')) {
                            $recipe->totalTime = $request->input('totalTime');
                        }
                        if ($request->filled('servings')) {
                            $recipe->servings = $request->input('servings');
                        }
                        if ($request->filled('calories')) {
                            $recipe->calories = $request->input('calories');
                        }
                        if ($request->filled('user_id')) {
                            $recipe->user_id = $request->input('user_id');
                        }
                        if ($recipe->save()) {
                            return new RecipeResource($recipe);
                        }
                    }
                }
                return response()->json([
                    'error' => true,
                    'msg' => 'Unauthorized',
                ], 401);
            }
            return response()->json([
                'error' => true,
                'msg' => 'User with id '. $request->input('user_id') .' was not found.',
            ], 404);
        }
        return response()->json([
            'error' => true,
            'msg' => 'Recipe with id '. $id .' was not found.',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Messages might be too infomrative though :D
        $recipe = Recipe::find($id);
        if ($recipe !== null) {
            $user = User::find($request->input('user_id'));
                if ($user !== null) { 
                    if ($user->api_token !== null) {
                        if ($user->api_token === $request->api_token) {
                            if($recipe->delete()) {
                                return new RecipeResource($recipe);
                            }
                        }
                    }
                    return response()->json([
                        'error' => true,
                        'msg' => 'Unauthorized',
                    ], 401);
                }
                return response()->json([
                    'error' => true,
                    'msg' => 'User with id '. $request->input('user_id') .' was not found.',
                ], 404);
            }
            return response()->json([
                'error' => true,
                'msg' => 'Recipe with id '. $id .' was not found.',
            ], 404);
        }
    }