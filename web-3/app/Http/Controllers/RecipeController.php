<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Http\Resources\Recipe as RecipeResource;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Gate;
//use Intervention\Image\Image;
use PDF;

class RecipeController extends Controller
{  
    public $lastSearchValue = "";
    /*
    *
    *
    */
    public function home()
    {
        // $recipes = Recipe::select('id', 'title', 'image', 'description')->get();
        $recipes = Recipe::join('users', 'user_id', '=', 'users.id')->select('recipes.id', 'title', 'image', 'description', 'name','cover_image','cover_image_pixel', 'user_id', 'is_public')->orderBy('recipes.created_at', 'desc')->get();
        foreach($recipes as $recipe)
        {
            // Substring the description for 100 characters and append ... indicating there's more.
            // only if the description has more characters than 100.
            if (strlen($recipe->description) >= 100)
            {
                $recipe->description = substr($recipe->description, 0, 100) . ' . . .';
            }
        }
        return view('home')->with('recipes', $recipes); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create a sorted array of all recipes to display.
        // display a max of x per page.
        $recipes = Recipe::join('users', 'user_id', '=', 'users.id')->select('recipes.id', 'title', 'image', 'name', 'recipes.created_at','cover_image', 'cover_image_pixel', 'user_id', 'is_public')->orderBy('recipes.created_at', 'desc')->paginate(2);
        return view('recipes')->with('recipes', $recipes);
    }
    public function search(Request $request){

        if(isset($request)){
            if($request->input('searchValue')){
                $this->validate($request,[
                    'searchValue' => 'required'
                ]);

                $searchValue = $request->input('searchValue');
                $this->lastSearchValue = $request->input('searchValue');
                //return 'searchValue '.$this->lastSearchValue.' test1';
            }
            else{
               // $this->lastSearchValue = 'hee';
                $searchValue = $this->lastSearchValue;
            
                //return 'searchValue '.$this->lastSearchValue.' test2';
            }
        }
        else{
            return 'problem 1';
        }

        $recipes = Recipe::join('users', 'user_id', '=', 'users.id')->select('recipes.id', 'title', 'image', 'name', 'recipes.created_at','cover_image', 'cover_image_pixel', 'user_id', 'is_public')->where('title', 'like', '%'.$searchValue.'%')->orderBy('recipes.created_at', 'desc')->paginate(2);
        return view('recipes')->with('recipes', $recipes);
    }

    public function exportPDF($id){

        $recipe = Recipe::findOrFail($id);
        $recipe->ingredients = explode(";", $recipe->ingredients);
        $recipe->directions = explode(";", $recipe->directions);

        //return view('recipePDF')->with('recipe', $recipe);

        //$pdf = PDF::loadView('recipePDF', compact('recipe'));
       // return $pdf->stream('Recipe.pdf');
        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('recipePDF', compact('recipe'))->stream('Recipe_'.$recipe->id.'.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth()->guest()){
            return view('createRecipe');
        }
        return redirect('/login')->with('error', 'Create permission denied: You can only create recipes when logged in!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth()->guest()){
            //check of alle info is ingevuld
            $this->validate($request,[
                'title' => 'required',
                'image' => 'required',
                'description' => 'required',
                'directions' => 'required',
                'ingredients' => 'required',
                'preperationTime' => 'required',
                'cookingTime' => 'required',
                'totalTime' => 'required',
                'servings' => 'required',
                'calories' => 'required',
                'cover_image' => 'image|nullable|max:1999' //Er kunnen errors ontstaan als de file >=2mb!
            ]);

            //uploaden van image
                if($request->hasFile('cover_image')){
                    $imageNoPixelation = $request->file('cover_image');
                    $img = Image::make($imageNoPixelation);
                    $fileName = time() . '.' . $imageNoPixelation->getClientOriginalExtension();
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();})
                        ->fit(500, 500);
                    $img->text('Recipiers', 500 / 2, 500 - (500 * 0.25), function($font) {
                            $font->file(public_path('/fonts/IndieFlower.ttf'));
                            $font->size(120);
                            $font->color('#fff');
                            $font->align('center');
                            $font->valign('top');
                    });
                    $img->save('storage/cover_images/' . $fileName);

                    // Pixelated
                    $fileNamePixel = 'pixelated-' . $fileName;
                    $img = Image::make($imageNoPixelation);
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();})
                        ->fit(500, 500);
                    $img->pixelate(40);
                    $img->save('storage/cover_images/' . $fileNamePixel);
                }
                else{
                    $fileName = 'NoImage.jpg';
                    $fileNamePixel = 'NoImage.jpg';
                }

            //maak het nieuwe recept
            $recipe = new Recipe;
            $recipe->title = $request->input('title');
            $recipe->image = $request->input('image');
            $recipe->description = $request->input('description');
            $recipe->directions = $request->input('directions');
            $recipe->ingredients = $request->input('ingredients');
            $recipe->preperationTime = $request->input('preperationTime');
            $recipe->cookingTime = $request->input('cookingTime');
            $recipe->totalTime = $request->input('totalTime');
            $recipe->servings = $request->input('servings');
            $recipe->calories = $request->input('calories');
            $recipe->user_id = auth()->user()->id;
            $recipe->cover_image = $fileName;
            $recipe->cover_image_pixel = $fileNamePixel;
            $recipe->save();

            //ga naar de pagina van het gemaakte recept
            return redirect('/recipes/'.$recipe->id)->with('success', 'Store permission granted: Your recipe has been saved');
            }
            return redirect('/login')->with('error', 'Store permission denied: You can only create recipes when logged in!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe = Recipe::findOrFail($id);
        $recipe->ingredients = explode(";", $recipe->ingredients);
        $recipe->directions = explode(";", $recipe->directions);

        return view('specific-recipe')->with('recipe', $recipe);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth()->guest()){
            $recipe = Recipe::findOrFail($id);
            if (Gate::allows('profile-owner', $recipe->user_id)) {
                return view('editRecipe')->with('recipe', $recipe);
            }    
            return redirect('/recipes/'.$id)->with('error', 'Edit permission denied: This recipe is not yours!');
        }
        return redirect('/login')->with('error', 'Edit permission denied: You can only edit recipes when logged in!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth()->guest()){
            $recipe = Recipe::findOrFail($id);
            if (Gate::allows('profile-owner', $recipe->user_id)) {
                
                //check of alle info is ingevuld
                $this->validate($request,[
                    'title' => 'required',
                    'image' => 'required',
                    'description' => 'required',
                    'directions' => 'required',
                    'ingredients' => 'required',
                    'preperationTime' => 'required',
                    'cookingTime' => 'required',
                    'totalTime' => 'required',
                    'cover_image' => 'image|nullable|max:1999' //Er kunnen errors ontstaan als de file >=2mb!
                    ]);
            
                    //uploaden van image
                    if($request->hasFile('cover_image')){  
                        
                    // $fileNameWithExtension = $request->file('cover_image')->getClientOriginalName();
                    // $fileName = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
                    // $extension = $request->file('cover_image')->getClientOriginalExtension();
                    // $fileNameStore = $fileName.'_'.time().'.'.$extension; //unique name
                    // $fileNameStorePixel = $fileName.'_'.time().'_Pixel.'.$extension;
                    // $path = $request->file('cover_image')->storeAs('storage/cover_images', $fileNameStore);
                    $imageNoPixelation = $request->file('cover_image');
                    $img = Image::make($imageNoPixelation);
                    $fileName = time() . '.' . $imageNoPixelation->getClientOriginalExtension();
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();})
                        ->fit(500, 500);
                    $img->text('Recipiers', 500 / 2, 500 - (500 * 0.25), function($font) {
                            $font->file(public_path('/fonts/IndieFlower.ttf'));
                            $font->size(120);
                            $font->color('#fff');
                            $font->align('center');
                            $font->valign('top');
                    });
                    $img->save('storage/cover_images/' . $fileName);

                    // Pixelated
                    $fileNamePixel = 'pixelated-' . $fileName;
                    $img = Image::make($imageNoPixelation);
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();})
                        ->fit(500, 500);
                    $img->pixelate(40);
                    $img->save('storage/cover_images/' . $fileNamePixel);

                    if($recipe->cover_image != 'NoImage.jpg'){ //er is een foto opgeslagen, delete het
                        Storage::delete('public/cover_images/'.$recipe->cover_image);
                        Storage::delete('public/cover_images/'.$recipe->cover_image_pixel);
                    }                 
                }

                //maak het nieuwe recept           
                $recipe->title = $request->input('title');
                $recipe->image = $request->input('image');
                $recipe->description = $request->input('description');
                $recipe->directions = $request->input('directions');
                $recipe->ingredients = $request->input('ingredients');
                $recipe->preperationTime = $request->input('preperationTime');
                $recipe->cookingTime = $request->input('cookingTime');
                $recipe->totalTime = $request->input('totalTime');
                $recipe->servings = $request->input('servings');
                $recipe->calories = $request->input('calories');
                $recipe->user_id = $recipe->user_id;
                if($request->hasFile('cover_image')){
                    $recipe->cover_image = $fileName;
                    $recipe->cover_image_pixel = $fileNamePixel;
                }
                $recipe->save();

                //ga naar de pagina van het gemaakte recept
                return redirect('/recipes/'.$id)->with('success', 'Update permission granted: Your recipe has been updated');
            }
            return redirect('/recipes/'.$id)->with('error', 'Update permission denied: This recipe is not yours!');
        }
        return redirect('/login')->with('error', 'Update permission denied: You can only update recipes when logged in!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get the current profile page or header currently and
        // redirect them there.
        if(!Auth()->guest()){
            $recipe = Recipe::findOrFail($id);
            if (Gate::allows('profile-owner', $recipe->user_id)) {
                if($recipe->cover_image != 'NoImage.jpg'){ //er is een foto opgeslagen
                    Storage::delete('public/cover_images/'.$recipe->cover_image);
                    Storage::delete('public/cover_images/'.$recipe->cover_image_pixel);
                }
                $recipe->delete();
                return redirect('/recipes')->with('success', 'Delete permission granted: Your recipe has been removed');
            }
            return redirect('/recipes/'.$id)->with('error', 'Delete permission denied: This recipe is not yours!');
        }
        return redirect('/login')->with('error', 'Delete permission denied: You can only delete recipes when logged in!');
    }
}
