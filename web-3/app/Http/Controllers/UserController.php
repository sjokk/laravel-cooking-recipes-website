<?php

namespace App\Http\Controllers;

use App\User;
use App\Exports;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    //
    // CRUD functionalities
    //
    public function destroy($id) {
        if (Gate::allows('isAdmin')) {
            $user = User::find($id);
            if ($user->delete()) {
                return redirect()->route('admin.users');
            }
        } else {
            return abort(404);
        }
    }

    // 
    //  Excel export related methods.
    //
    public function export()
    {
        if (Gate::allows('isAdmin')) {
            $users = User::all();
            return view('UserController.user-export')->with('users', $users);
        } else {
            return abort(404);
        }
    }

    public function export_users()
    {
        if (Gate::allows('isAdmin')) {
            return Excel::download(new UsersExport, 'users.xlsx');
        } else {
            return abort(404);
        }
    }
}
