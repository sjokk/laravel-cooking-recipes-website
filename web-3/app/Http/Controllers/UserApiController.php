<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserApiController extends Controller
{
    //
    //  API related methods
    //
    // Shows all entries, limited at 15 per page.
    public function index()
    {
        return UserResource::collection(User::paginate(15)); 
    }

    // Shows a specific user for id and returns a json message if the user is not found.
    public function show($id)
    {
        $user = User::find($id);
        if ($user !== null) {
            return new UserResource($user);
        } 
        return response()->json([
            'error' => true,
            'msg' => 'User with id '. $id .' was not found.',
        ], 404);
    }

    // Creates a new user based on the provided input from the post request.
    public function store(Request $request)
    {
        // Error handling ?
        // Validation ?
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->is_public = 0;
        if ($user->save()) {
            return new UserResource($user);
        }
    }

    // Deletes the user with id $id, returns json message if user is not found.
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        if ($user !== null) {
            if ($user->api_token !== null) {
                if ($user->api_token === $request->api_token) {
                    if ($user->delete()) {
                        return new UserResource($user);
                    }
                }
            }
            return response()->json([
                'error' => true,
                'msg' => 'Unauthorized',
            ], 401);
        }
        return response()->json([
            'error' => true,
            'msg' => 'User with id '. $id .' was not found.',
        ], 404);
    }

    // Updates the input provided in the put request for user with id $id.
    // Returns an appropriate json response if user was not found.
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user !== null) {
            // Only update fields that are provided and with values.
            // No clue if there is any way of validating this.
            if ($user->api_token !== null) {
                if ($user->api_token === $request->api_token) {
                    if ($request->filled('name')) {
                        $user->name = $request->input('name');
                    }
                    if ($request->filled('is_public')) {
                        $user->is_public = $request->input('is_public');
                    }
                    if ($user->save()) {
                        return new UserResource($user);
                    }
                }
            }
            return response()->json([
                'error' => true,
                'msg' => 'Unauthorized',
            ], 401);
        }
        return response()->json([
            'error' => true,
            'msg' => 'User with id '. $id .' was not found.',
        ], 404);
    }
}
