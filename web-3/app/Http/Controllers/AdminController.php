<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{
    //
    public function index() {
        if (Gate::allows('isAdmin')) {
            return view('AdminController.index');
        } else {
            return abort(404);
        }  
    }

    public function users() {
        if (Gate::allows('isAdmin')) {
            return view('AdminController.user-table')->with('users', User::all());
        } else {
            return abort(404);
        }  
    }
}
