<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Recipe;
use App\User;
use Image;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Remove middleware in general and add it to routes.
        // that require it.
        // $this->middleware('auth');
    }

    /**
     * Show the application profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if ($id !== null) // The ID searched for can be a different user.
        {
            $user = User::findOrFail($id);
            // Test if the profile for this id is not private.
            // If it is, test if the profile the person wants to view
            // is his or her.
            if ($user->is_public == 1 || Gate::allows('profile-owner', $id)) 
            {
                $recipeCount = Recipe::where('user_id', $id)->count();
                $recipes = Recipe::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(5);
                $lastSharedRecipe = Recipe::where('user_id', $id)->max('created_at');
                $data = [
                    'userRecipes' => $recipes,
                    'recipeCount' => $recipeCount,
                    'lastShared' => $lastSharedRecipe,
                    'user' => $user
                ];
                return view('ProfileController.profile-normal')->with('data', $data);
            }
            else
            {
                // The requested profile was nor public nor the authenticated user's profile.
                return view('ProfileController.not-public');
                /////////////////////////////////////////
                // Maybe return a pretty view later on //
                /////////////////////////////////////////
            }
        }
        else 
        {
            // Route the user to his/her own page if s/he is logged in.
            if (Auth::check())
            {
                return $this->index(Auth::id());
            }
            else
            {
                // Route the user to the log in page.
                return redirect()->route('home');
            }
        }
    }

    public function edit($id)
    {
        if (Gate::allows('profile-owner', $id))
        {
            $recipeCount = Recipe::where('user_id', $id)->count();
            $recipes = Recipe::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(5);
            $lastSharedRecipe = Recipe::where('user_id', $id)->max('created_at');
            $user = User::findOrFail($id);
            $data = [
                'userRecipes' => $recipes,
                'recipeCount' => $recipeCount,
                'lastShared' => $lastSharedRecipe,
                'user' => $user
            ];
            return view('ProfileController.profile-edit')->with('data', $data);
        }
        // If the user is not the profile owner, return the user to it's own profile.
        // return $this->edit(Auth::id());
        return abort(404);
    }

    /**
     * Update the user's profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        // This will always return the authenticated user
        // but we want to do different if the user is an admin.
        // then we want to actually change the current user we 
        // are working on.
        $user = $request->user();
        // Test if the user is an admin, then we want to
        // overwrite the user variable with the user for the
        // posted id.
        if ($user->isAdmin()) {
            $user = User::find($id);
        }
        $user->name = $request->input('name');
        $user->is_public = $request->has('is_public');

        if($request->hasFile('avatar'))
        {
            $avatar = $request->file('avatar');
            $fileName = time() . '.' . $avatar->getClientOriginalExtension();
            $width = 200;
            $height = 200;
            $img = Image::make($avatar)
                // Resize the image to a height of 200px and keep the width auto sized.
                ->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();})
                ->fit($width, $height); // Make sure the image will fit our 200 by 200 dimensions.
            
            $img->text('Recipiers', $width / 2, $height - ($height * 0.2), function($font) {
                $font->file(public_path('/fonts/IndieFlower.ttf'));
                $font->size(24);
                $font->color('#fff');
                $font->align('center');
                $font->valign('top');
            });

            // Check if the current avatar is not default so we delete the old image
            // and upload the new only.
            $previousProfileImg = $user->avatar;
            if ($previousProfileImg !== "default.jpg") {
                File::delete(public_path('/uploads/avatars/' . $previousProfileImg));
            }
            $img->save(public_path('/uploads/avatars/' . $fileName));
            $user->avatar = $fileName;
        }
        $user->save();

        // Redirect to named route to clear form resubmission.
        return redirect()->route('profile', $user->id);
    }

    public function generateToken($id) {
        $user = Auth::user();
        // Check if the authenticated user matches the inserted id.
        if ($user->id === $id) {
            // Execute regular token generation for the authenticated user itself.
            $user->api_token = str_random(60);
            if ($user->save()) {
                return redirect()->route('profile', $user->id);
            }
        } else {
            // Check if the user trying to generate a token is an admin,
            // otherwise abort.
            if (Gate::allows('isAdmin')) {
                $user = User::find($id);
                $user->api_token = str_random(60);
                if ($user->save()) {
                    return redirect()->route('profile', $user->id);
                }
            } else {
                return abort(404);
            }

        }
        
    }
}