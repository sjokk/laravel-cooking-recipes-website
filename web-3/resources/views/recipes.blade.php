@extends('layouts.app')

@section('title', 'Recipes')

@section('pageStyling', asset('css/recipes.css'))

@section('content')
    @component('components.display-image')
        @slot('displayImgUrl')
            https://3.bp.blogspot.com/-zFc1GzhF5fg/TrukYPggIOI/AAAAAAAAQ38/AvHWQWFJrVM/s1600/Mooie-eten-achtergronden-leuke-hd-eten-wallpapers-afbeelding-voedsel-plaatje-foto-15.jpg
        @endslot
    @endcomponent
    <h1>Available recipes</h1>
    @if(false)
    <div class="container">
        
        {!! Form::open(['action' => 'RecipeController@search', 'method' => 'GET', 'enctype' => 'multipart/form-data']) !!}
        <div class="row">
            <div class="col-sm">
            {{Form::text('searchValue', '', ['class' => 'form-control', 'placeholder' => 'recipe name...'])}}
            </div>
            <div class="col-sm">
            {{Form::submit('Search', ['class' => 'btn btn-primary'])}}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <br>
    @endif
    @if (count($recipes) > 0)
        <div class="row">
            <div class="col">

            </div>
            <div class="col">
                <h4>Recipe Title</h4>
            </div>
            <div class="col">
                <h4>Created By</h4>
            </div>
            <div class="col">
                <h4>Uploaded At</h4>
            </div>
            @can('isAdmin')
                <div class="col">
                    <h4>Actions</h4>
                </div>
            @endcan
        </div>
        {{-- Display picture, a title, total time, created at in row form. --}}
        @foreach ($recipes as $recipe)
            <div class="row table-row">
                @component('components.recipe-display')
                    @slot('foodImage')
                        {{$recipe->cover_image}}
                    @endslot
                    @slot('foodImagePixel')
                        {{$recipe->cover_image_pixel}}
                    @endslot 
                    @slot('id')
                        {{ $recipe->id }}
                    @endslot
                    @slot('recipeTitle')
                        {{ $recipe->title }}
                    @endslot
                    <div class="col">
                        @if( $recipe->is_public == 1)
                            <a class="a-profile" href="{{ route('profile', $recipe->user_id) }}">
                                {{ $recipe->name }}
                            </a>
                        @else
                            {{ $recipe->name }}
                        @endif
                    </div>
                    <div class="col">
                        {{ $recipe->created_at }}
                    </div>
                    @can('isAdmin')
                        <div class="col">
                            <a href="{{ route('recipes.edit', $recipe->id) }}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="{{ route('recipe.delete', $recipe->id) }}" class="btn btn-danger btn-sm">Delete</a>
                        </div>
                    @endcan
                @endcomponent
            </div>
        @endforeach
        {{ $recipes->links() }}
    @else
        <div>
            <h4>
                Unfortunately there are no recipes.
            </h4>
        </div>
    @endif
@endsection