<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{$recipe->title}}</title>
    <!-- bootstrap werkt niet met deze pdf methode! -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style>
    #wrapper {
        width: 80%;
    }
    
    #first {
        width: 35%;
        float:left; /* add this */
    }
    #second {
        overflow: hidden; /* if you don't want #second to wrap below #first */
    }
    
    </style>

</head>
<body>
<div class="container">
    <div class="row">
        <h2>
            {{ $recipe->title }}
        </h2>
    </div>
    <hr>
    <div id="wrapper">
        <div id="first">            
            <img  class="img-thumbnail rounded recipe-img" src="./storage/cover_images/{{ $recipe->cover_image }}" >  
        </div>
        <div id="second" class="col-sm-4">  
            <h3> Information: </h3>  
            <p>       
                Servings: {{ $recipe->servings}} <br>
                Calories: {{ $recipe->calories}} <br><br>
                Total-time: {{ $recipe->totalTime}} <br>
                Cooking-time: {{ $recipe->cookingTime}} <br>
                Preperation-time: {{ $recipe->preperationTime}} <br>
            </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm">
            <h3>
                Ingredients:
            </h3>
            <p>
                <ol>
                    @foreach($recipe->ingredients as $ingredient)
                        @if ($ingredient !== "")
                        <li>{{ $ingredient }}</li>
                        @endif
                    @endforeach
                </ol>
            </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm">
            <h3>
                Directions:
            </h3>
            <p>
                <ol>
                    @foreach($recipe->directions as $direction)
                        @if ($direction !== "")
                        <li>{{ $direction }}</li>
                        @endif
                    @endforeach
                </ol>
            </p>
        </div>
    </div>
</div>
</body>
</html>
