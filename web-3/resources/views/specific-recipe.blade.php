@extends('layouts.app')

@section('title', $recipe->title)

@section('pageStyling', asset('css/specific-recipe.css'))

@section('content')
    <h2>
        {{ $recipe->title }}
    </h2>
    <div class="row content-row">
        <div class="col-md-3">
            <img class="img-thumbnail rounded recipe-img" src="../storage/cover_images/{{ $recipe->cover_image }}" alt="recipe image">
        </div>
        <div class="col-md-3">
            <h4>Description</h4>
            {{ $recipe->description }}
        </div>
        <div class="col-md-3">
            <div>
                <div>
                    <div>
                        <h4>Estimated Time Required</h4>
                    </div>
                    <div>Total: {{ $recipe->totalTime }}</div>
                    <div>Preperation: {{ $recipe->preperationTime }}</div>
                    <div>Cooking: {{ $recipe->cookingTime }}</div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div>
                <b>Total # of servings:</b> {{ $recipe->servings }}
            </div>
            <div>
                <b>Calories per serving:</b> {{ $recipe->calories }}
            </div>
        </div>
    </div>
    {{-- The ingredient list items are made clickable just in case.
        We could be fancy and add an option for the recipe 
        uploader to link to specific web shops or w/e where 
        s/he purchases the ingredients from. --}}
    <div class="row">
        <div class="col-md-3">
            <h4 class="list-head">Ingredients</h4>
            <div class="list-group">
                @foreach($recipe->ingredients as $ingredient)
                    @if ($ingredient !== "")
                    <a href="#" class="list-group-item list-group-item-action">{{ $ingredient }}</a>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-3">
            <h4 class="list-head">Directions</h4>
            <ul class="list-group list-group-flush">
                @foreach($recipe->directions as $direction)
                    @if ($direction !== "")
                        <li class="list-group-item">{{ $direction }}</li>
                    @endif
                @endforeach
            </ul>
        </div>
        @if(!Auth::guest())
        <hr>
        <div class="col-md-3">
        @can('profile-owner', $recipe->user_id)
                <a href="{{ route('recipes.edit', $recipe->id) }}" class="btn btn-primary btn-sm">Edit Recipe</a>
                <a href="{{ route('recipe.delete', $recipe->id) }}" class="btn btn-danger btn-sm">Delete</a>
        @endcan
                <a href="{{ route('recipePDF', $recipe->id) }}" class="btn btn-light btn-sm">PDF</a>
            </div>
        </div>   
        @endif
    </div>
@endsection