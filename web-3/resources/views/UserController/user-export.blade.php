@extends('layouts.app')

@section('title', 'User Export')

@section('content')
    <a href="{{ route('user.export') }}" class="btn btn-secondary">
        Export Users
    </a>
    @Component('UserController.user-table', ['users' => $users])
    @endComponent
@endsection