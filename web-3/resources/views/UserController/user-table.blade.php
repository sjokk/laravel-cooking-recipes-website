<table class="table">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">Name</th>
        <th scope="col">E-mail</th>
        <th scope="col">Created at</th>
        <th scope="col">Updated at</th>
        <th scope="col">Is a public profile</th>
    </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->created_at->format('Y-m-d') }}</td>
            <td>{{ $user->updated_at->format('Y-m-d') }}</td>
            <td>
                @if($user->is_public === 1)
                    Yes
                @else
                    No
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>