@extends('layouts.app')

@section('title', 'Admin')

@section('pageStyling', asset('css/home.css'))

@section('content')
    @component('components.display-image')
        @slot('displayImgUrl')
            https://femmehub.com/wp-content/uploads/2015/04/foods-wallpaper.jpg
        @endslot
    @endcomponent
    {{-- Yield content for the admin page --}}
    @yield('admin-content')
@endsection