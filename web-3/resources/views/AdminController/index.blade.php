@extends('AdminController.home')
@section('admin-content')
    <div class="row">
        <div class="col">
            <h2>Users</h2>
            <a href="{{ route('admin.users') }}" class="btn btn-small btn-danger">Users overview</a>
        </div>
        <div class="col">
            <h2>Recipes</h2>
            <a href="{{ route('recipes.index') }}" class="btn btn-small btn-danger">Recipes overview</a>
        </div>
    </div>
@endsection