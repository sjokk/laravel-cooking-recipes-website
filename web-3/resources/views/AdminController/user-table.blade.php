@extends('AdminController.home')
@section('admin-content')
    <h1 style="text-align: center;">Users</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Name</th>
            <th scope="col">E-mail</th>
            <th scope="col">Created at</th>
            <th scope="col">Updated at</th>
            <th scope="col">Is a public profile</th>
            {{-- Yield extra th for admin table --}}
            @can('isAdmin')
                <th scope="col">Options</th>
            @endcan
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->created_at->format('Y-m-d') }}</td>
                <td>{{ $user->updated_at->format('Y-m-d') }}</td>
                <td>
                    @if($user->is_public === 1)
                        Yes
                    @else
                        No
                    @endif
                </td>
                {{-- Yield extra td --}}
                @can('isAdmin')
                    <td>
                        <div>
                            {{-- Button that goes to the editing part of the user profile --}}
                            <a class="btn btn-primary btn-sm" href="{{ route('profile.edit', $user->id) }}">Edit</a>
                            {{-- Calls the destroy method of the controller for user with id --}}
                            <a class="btn btn-danger btn-sm" href="{{ route('admin.user.delete', $user->id) }}">Delete</a>
                        </div>
                    </td>
                @endcan
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection