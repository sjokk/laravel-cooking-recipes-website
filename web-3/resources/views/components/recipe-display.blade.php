
<div class="col">
   <!-- <canvas id={{ $foodImage }} onmouseover="pixel(this)" onmouseout="normal(this)">
    </canvas>-->
<img id="{{ asset('storage/cover_images/' . $foodImagePixel) }}" src="{{ asset('storage/cover_images/' . $foodImage) }}" onmouseenter="change(this)" onmouseleave="change(this)" class="img-thumbnail rounded">
</div>
<div class="col">
    <h4>
        <a class="recipeDisplayAnchor" href="{{ route('recipes.show', $id) }}">
            {{ $recipeTitle }}
        </a>
    </h4>
</div>
<script>
var normalImageTemp;
    function change(image){   
        if(!image.src.includes("NoImage.jpg")){    
            if(image.src.includes(image.id)){
                console.log("normal");
                image.src = normalImageTemp;
                normalImageTemp = "";
            }
            else{
                console.log("pixel");
                normalImageTemp = image.src;
                image.src = image.id;
            }
        }
    }
</script>
{{ $slot }}

<!--
<script>
//PROBLEEM: IEDERE FOTO OP DEZELFDE PAGINA IS HETZELFDE!
    var value = 10; //pixel percentage  
    img = new Image;
    img.src = "{{$foodImage}}";         
    img.onload = normal(document.getElementById("{{$foodImage}}"));
    function pixel(Canvas) {
        img = new Image;
        img.src = "{{$foodImage}}";
        ctx = Canvas.getContext('2d');
        // set picture size according to Canvas
        img.width = Canvas.width;
        img.height = Canvas.height;

        /// calculate the factor
        var fw = (img.width / value)|0,
            fh = (img.height / value)|0;
        
        /// turn off image smoothing (prefixed in some browsers)
        ctx.imageSmoothingEnabled = false;
        ctx.mozImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        
        /// draw mini-version of image
        ctx.drawImage(img, 0, 0, fw, fh);
        
        /// draw the mini-version back up, voila, pixelated
        ctx.drawImage(Canvas, 0, 0, fw, fh, 0, 0, img.width, img.height);
    }
    function normal(Canvas){
        img = new Image;
        img.src = "{{$foodImage}}";
        ctx = Canvas.getContext('2d');
        // set picture size according to Canvas
        img.width = Canvas.width;
        img.height = Canvas.height;

        ctx.drawImage(img, 0, 0, img.width, img.height);
    }
</script>
-->