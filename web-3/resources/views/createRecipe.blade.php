@extends('layouts.app')

@section('content')
<div class="container">
    <h1><b>Create your new recipe here!</b></h1>
    {!! Form::open(['action' => 'RecipeController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <hr>
    <h2>Basic information</h2>
    <hr>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm">
                    <div class="form-group">
                        {{Form::label('title', 'Title')}}
                        {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Name of recipe'])}}
                    </div>
                    <div class="form-group">
                        {{Form::file('cover_image')}}
                    </div>
                    <div class="form-group">
                        {{Form::label('image', 'Image URL')}}
                        {{Form::text('image', '', ['class' => 'form-control', 'placeholder' => 'Image URL'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('description', 'Description')}}
                        {{Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'Description of recipe'])}}
                    </div>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                        {{Form::label('preperationTime', 'Preperation Time')}}
                        {{Form::time('preperationTime', '', ['class' => 'form-control', 'placeholder' => 'PreperationTime of recipe'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('cookingTime', 'Cooking Time')}}
                        {{Form::time('cookingTime', '', ['class' => 'form-control', 'placeholder' => 'CookingTime of recipe'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('totalTime', 'Total Time')}}
                        {{Form::time('totalTime', '', ['class' => 'form-control', 'placeholder' => 'TotalTime of recipe'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('servings', 'Servings')}}
                        {{Form::number('servings', '', ['class' => 'form-control', 'placeholder' => 'Total servings'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('calories', 'Calories')}}
                        {{Form::number('calories', '', ['class' => 'form-control', 'placeholder' => 'Total calories'])}}
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <hr>   
    <h2>Ingredients and directions</h2>
    <hr>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm">
                    <div class="form-group">
                        {{Form::label('ingredients', 'Ingredients')}}
                        {{Form::textarea('ingredients', '', ['class' => 'form-control', 'placeholder' => 'Ingredients of recipe'])}}
                    </div>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                        {{Form::label('directions', 'Directions')}}
                        {{Form::textarea('directions', '', ['class' => 'form-control', 'placeholder' => 'Directions of recipe'])}}
                    </div>    
                </div>
            </div> 
        </div>   
    </div> 
    <hr>
    {{Form::submit('Submit recipe', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
</div>  
@endsection

