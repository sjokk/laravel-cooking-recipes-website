@extends('ProfileController.profile')

@section('user-data')
    {!! Form::open(['action' => ['ProfileController@update', $data['user']->id], 'method' => 'POST', 'files' => true]) !!}
        <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', $data['user']->name, ['class' => 'form-control', 'placeholder' => 'Your name']) }}
        </div>
        <div class="form-group">
            {{ Form::label('is_public', 'Public profile:') }}
            @if($data['user']->is_public == 0)
                {{ Form::checkbox('is_public', 'value') }}
            @else
                {{ Form::checkbox('is_public', 'value', true) }}
            @endif
        </div>
        <div class="form-group">
            {{ Form::label('avatar', 'Profile Picture') }}
            {{ Form::file('avatar') }}
        </div>
        {{ Form::hidden('_method', 'PUT') }}
        {{ Form::submit('Update Details', ['class' => 'btn btn-primary btn-sm']) }}
        <a href="{{ route('profile', $data['user']->id) }}" class="btn btn-danger btn-sm" >Cancel</a>
    {!! Form::close() !!}
@endsection