@extends('layouts.app')

@section('title') 
  Profile of {{ $data['user']->name }}
@endsection

@section('pageScript', asset('js/user-profile.js'))

@section('pageStyling', asset('css/profile.css'))

@section('content')
  <div class="row">
    <div class="col">
      <div id="user">
        <h1>{{ $data['user']->name }}</h1>
        <img id="profilePicture" src="{{ asset('uploads/avatars/' . $data['user']->avatar) }}" alt="{{ $data['user']->name }}'s Profile Picture." class="img-thumbnail">
      </div>
    </div>
    <div class="col">
      <div id="user-data">
        @yield('user-data')
      </div>
      <div id="button-div">
        
      </div>
    </div>
    <div class="col">
      <div>
        <div class="stats">
          <div class="stat-label">
            Currently shared: 
          </div>
          <div class="stat-container">
          @if ($data['recipeCount'] == 1)
            {{ $data['recipeCount'] }} recipe
          @else
            {{ $data['recipeCount'] }} recipes
          @endif
          </div>
          <div class="stat-label">
              Last Time Shared: 
          </div>
          <div class="stat-container">
            @if($data['lastShared'] !== null)
              {{ $data['lastShared'] }}
            @else
              <b>Never</b>
            @endif
          </div>
        </div>
      </div>
    </div> 
  </div>
  <div id="recipeRow" class="row">
    <div class="col">
      @if ($data['recipeCount'] > 0 )
        <h1 id="recipeHeader">Shared Recipes</h1>
        <table id="recipe-table" class="table table-dark">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">Title</th>
              <th scope="col">Created At</th>
              <th scope="col">Updated At</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($data['userRecipes'] as $userRecipe)
              <tr onclick="location.href='{{ route('recipes.show', $userRecipe->id) }}'" class="recipe-row">
                <td height="15%" width="15%">
                    <img id="{{ asset('storage/cover_images/' . $userRecipe->cover_image_pixel) }}" src="{{ asset('storage/cover_images/' . $userRecipe->cover_image) }}" onmouseenter="change(this)" onmouseleave="change(this)" class="img-thumbnail rounded" >
                   <!-- <img src="{{ $userRecipe->image }}" style="height: 100px" class="img-thumbnail rounded"> -->
                </td>
                <td>{{ $userRecipe->title }}</td>
                <td>{{ $userRecipe->created_at->format('Y-m-d') }}</td>
                <td>{{ $userRecipe->updated_at->format('Y-m-d') }}</td>
                {{-- These buttons should only be visible by the user itself. --}}
                @can('profile-owner', $data['user']->id)
                  <td>
                    <div class="row">
                      <a href="{{ route('recipes.edit', $userRecipe->id) }}" class="btn btn-primary" >Edit</a>
                      {!!Form::open(['action' => ['RecipeController@destroy', $userRecipe->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                      {{Form::hidden('_method', 'DELETE')}}
                      {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                      {!!Form::close()!!}
                      <a href="{{ route('recipePDF', $userRecipe->id) }}" class="btn btn-light" >PDF</a>
                    </div>
                  </td>
                @endcan
              </tr>
              @endforeach
          </tbody>
        </table>
        {{ $data['userRecipes']->links() }}
      @else
        <div>
          <h1>
            No recipes are shared!
          </h1>
        </div>
      @endif
      {{-- This button should be only available to the user self. --}}
      @can('profile-owner', $data['user']->id)
        <a href="{{ route('recipes.create') }}" class="btn btn-secondary">Share a Recipe</a>
      @endcan
    </div>
</div>
<script>
var normalImageTemp;
    function change(image){   
        if(!image.src.includes("NoImage.jpg")){    
            if(image.src.includes(image.id)){
                console.log("normal");
                image.src = normalImageTemp;
                normalImageTemp = "";
            }
            else{
                console.log("pixel");
                normalImageTemp = image.src;
                image.src = image.id;
            }
        }
    }
</script>
@endsection