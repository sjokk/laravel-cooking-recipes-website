@extends('ProfileController.profile')

@section('user-data')
<table>
    <thead>
        <tr>
            <th></th>
            <th>Data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Name: </td>
            <td>{{ $data['user']->name }}</td>
        </tr>
        <tr>
            <td>Public profile: </td>
            <td>
                @if( $data['user']->is_public == 0)
                    No
                @else
                    Yes
                @endif
            </td>
        </tr>
        @can('profile-owner', $data['user']->id)
            <tr>
                <td>
                    API Token
                </td>
                <td>
                    @if ($data['user']->api_token === null)
                        <a href="{{ route('profile.generate', $data['user']->id) }}" class="btn btn-secondary btn-sm">Generate</a>
                    @else
                        <button id="show-token-button" class="btn btn-danger btn-sm">Show</button>
                    @endif 
                </td>
            </tr>
            <tr>
                <td></td>
                <td id="api-td"></td>
            </tr>
            <script>
                (() => {
                    var btn = document.querySelector("#show-token-button");
                    var td = document.querySelector("#api-td");
                    btn.addEventListener("click", () => {
                        if (btn.innerHTML == "Show") {
                            td.innerHTML = "{{ $data['user']->api_token }}";
                            btn.innerHTML = "Hide";
                        } else {
                            td.innerHTML = "";
                            btn.innerHTML = "Show";
                        }
                        
                    })
                })();

            </script>
        @endcan
    </tbody>
</table>
{{-- Button should only be visible by the owner. --}}
@can('profile-owner', $data['user']->id)
    <a class="btn btn-secondary btn-sm" href="{{ route('profile.edit', $data['user']->id) }}">Change Details</a>
@endcan
@endsection