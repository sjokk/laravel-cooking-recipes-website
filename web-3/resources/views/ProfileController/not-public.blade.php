@extends('layouts.app')

@section('title') 
    Not public
@endsection

@section('pageScript', asset('js/user-profile.js'))

@section('pageStyling', asset('css/profile.css'))

@section('content')
<div class="row">
    <div class="col">
        Unfortunately the profile you try to view is not public!
    </div>
</div>
@endsection