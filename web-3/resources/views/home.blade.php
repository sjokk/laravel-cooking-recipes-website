@extends('layouts.app')

@section('title', 'Recipiers Home')

@section('pageStyling', asset('css/home.css'))

@section('content')
    @component('components.display-image')
        @slot('displayImgUrl')
            https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fstatic01.nyt.com%2Fimages%2F2014%2F01%2F06%2Fdining%2Frecipes-wildmushroomstew%2Frecipes-wildmushroomstew-superJumbo.jpg&f=1
        @endslot
    @endcomponent
    <div class="row">
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
        <div class="col"><img src="" alt="" class="rounded-circle">Food category img</div>
    </div>
    @if (count($recipes) > 0)
        <div class="row">
            {{-- Always start with a new row as we have recipes in the db. --}}
            @for($i = 0; $i < count($recipes); $i++)
                @if( $i != 0 && $i % 3 == 0)
                    {{-- If we placed 3 recipes in the row, end the previous row and start a new row. --}}
                    </div>
                    <div class="row">
                    {{-- Our controller can ensure we have a max of x rows each with at most 3 columns of recipes.. --}}
                @endif
                {{-- For every recipe we want to use the same template, and inject appropriate data. --}}
                <div class="col-4">
                    @Component('components.recipe-display')                      
                         @slot('foodImage')
                            {{$recipes[$i]->cover_image}}
                        @endslot
                        @slot('foodImagePixel')
                            {{$recipes[$i]->cover_image_pixel}}
                        @endslot 
                        @slot('id')
                            {{ $recipes[$i]->id }}
                        @endslot
                        @slot('recipeTitle')
                            {{ $recipes[$i]->title }}
                        @endslot
                        <div class="col">{{ $recipes[$i]->description }}</div>
                        <div class="col">
                            <b>
                                Created by: 
                                @if ($recipes[$i]->is_public == 1)
                                    <a class="a-profile" href="{{ route('profile', $recipes[$i]->user_id) }}">
                                        {{ $recipes[$i]->name }}
                                    </a>
                                @else
                                    {{ $recipes[$i]->name }}
                                @endif
                            </b>
                        </div>
                    @endcomponent
                </div>
            @endfor
        </div>
    @endif
    
@endsection