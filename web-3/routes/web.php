<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', 'RecipeController@home')->name('home');

// Recipes
Route::resource('recipes', 'RecipeController');

Auth::routes();

// All those that require authentication.
Route::middleware(['auth'])->group(function() {
    Route::get('profile/{id}/edit', 'ProfileController@edit')->name('profile.edit');
    Route::put('profile/{id}/update', 'ProfileController@update')->name('profile.update');
    Route::get('profile/{id}/generate-token', 'ProfileController@generateToken')->name('profile.generate');
    Route::get('users/export/export-all', 'UserController@export_users')->name('user.export');
    Route::get('users/export', 'UserController@export')->name('export');
    Route::get('admin', 'AdminController@index')->name('admin');
    Route::get('admin/users', 'AdminController@users')->name('admin.users');
    Route::get('admin/users/{id}/delete', 'UserController@destroy')->name('admin.user.delete');
    Route::get('recipe/{id}/delete', 'RecipeController@destroy')->name('recipe.delete');
});

// User Profile
Route::get('profile/{id?}', 'ProfileController@index')->name('profile');


// Exports
Route::get('recipes/{id}/pdf', 'RecipeController@exportPDF')->name('recipePDF');
Route::get('search/recipes', array('uses' => 'RecipeController@search'));